from django.contrib import admin
from .models import Payment, MBWay, CreditCard

# Register your models here.

admin.site.register(Payment)
admin.site.register(MBWay)
admin.site.register(CreditCard)
