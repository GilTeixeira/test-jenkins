import json
from uuid import UUID
from django.utils import timezone
from django.http import JsonResponse
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError, FieldError
from .models import Payment, CreditCard, MBWay

# Create your views here.


@method_decorator(csrf_exempt, name='dispatch')
class PaymentView(View):

    def get(self, request):
        payments = [payment.serialize() for payment in Payment.objects.all()]
        parameters = {field_name: value for field_name,
                      value in request.GET.items()}

        try:
            payments = [payment.serialize()
                        for payment in Payment.objects.filter(**parameters)]
        except ValidationError as error:
            return JsonResponse({'msg': error.message_dict}, status=400)
        except FieldError as error:
            return JsonResponse({'msg': {'Invalid Parameter': str(error)}}, status=400)

        return JsonResponse(payments, safe=False)

    def post(self, request):
        post_body = json.loads(request.body)

        payment_method = post_body.get('payment_method')

        if payment_method == 'credit_card':
            payment = CreditCard()
            payment.number = post_body.get('number')
            payment.name = post_body.get('name')
            payment.expiration_month = post_body.get('expiration_month')
            payment.expiration_year = post_body.get('expiration_year')
            payment.cvv = post_body.get('cvv')

        elif payment_method == 'mbway':
            payment = MBWay()
            payment.phone_number = post_body.get('phone_number')

        else:
            return JsonResponse(
                {"errors": [{"payment_method": "Invalid Payment Method"}]}, status=400
            )

        payment.amount = post_body.get('amount')
        payment.payment_method = payment_method
        payment.status = post_body.get('status')
        payment.settled_at = None

        try:
            payment.full_clean()
            payment.save()

        except ValidationError as error:
            return JsonResponse({'errors': error.message_dict}, status=400)

        return JsonResponse(payment.serialize(), status=201)


@method_decorator(csrf_exempt, name='dispatch')
class PaymentSpecificView(View):

    def get(self, request, payment_id):
        try:
            UUID(payment_id, version=4)
        except ValueError:
            return JsonResponse(
                {'errors': [{'payment_id': 'It must be a valid uuid4'}]},
                status=400
            )
        try:
            payment = Payment.objects.get(payment_id=payment_id)

            if payment.payment_method == 'credit_card':
                payment = CreditCard.objects.get(payment_id=payment_id)
            elif payment.payment_method == 'mbway':
                payment = MBWay.objects.get(payment_id=payment_id)

        except Payment.DoesNotExist:
            return JsonResponse(
                {'errors': [{'payment_id':
                             f'It does not exist an payment with payment_id={payment_id}'}]},
                status=400
            )

        return JsonResponse(payment.serialize(), safe=False)

    def delete(self, request, payment_id):
        try:
            UUID(payment_id, version=4)
        except ValueError:
            return JsonResponse(
                {'errors': [{'payment_id': 'It must be a valid uuid4'}]},
                status=400
            )
        try:
            payment = Payment.objects.get(
                payment_id=payment_id)
        except Payment.DoesNotExist:
            return JsonResponse(
                {'errors': [{'payment_id':
                             f'It does not exist an payment with payment_id={payment_id}'}]},
                status=400
            )

        payment.delete()

        return JsonResponse({}, status=204)


@method_decorator(csrf_exempt, name='dispatch')
class PaymentSettleView(View):

    def patch(self, request, payment_id):
        try:
            UUID(payment_id, version=4)
        except ValueError:
            return JsonResponse(
                {'errors': [{'payment_id': 'It must be a valid uuid4'}]},
                status=400
            )

        try:
            payment = Payment.objects.get(payment_id=payment_id)
        except Payment.DoesNotExist:
            return JsonResponse(
                {'errors': [
                    {'payment_id': f'It does not exist an payment with payment_id={payment_id}'}]},
                status=400
            )
        if payment.status != 'error':
            return JsonResponse({'errors': ["Can only settle payments with errors"]}, status=400)

        put_body = json.loads(request.body)
        payment.status = 'settled'
        payment.settled_amount = put_body.get('settled_amount')
        payment.settled_at = timezone.now()

        try:
            payment.full_clean()
            payment.save()

        except ValidationError as error:
            return JsonResponse({'errors': error.message_dict}, status=400)

        return JsonResponse(payment.serialize(), status=200)
