from django.test import TestCase
from ..models import Payment, CreditCard, MBWay

# Create your tests here.

class PaymentTest(TestCase):

    def setUp(self):
        CreditCard.objects.create(
            amount=100, payment_method='credit_card',
            status='success',
            number='4111111111111111', name='John Doe', expiration_month='10',
            expiration_year='2042', cvv='123'
        )

        MBWay.objects.create(
            amount=100, payment_method='mbway',
            status='success',
            phone_number='910000000'
        )

    def test_payments(self):
        self.assertEqual(len(CreditCard.objects.all()), 1)
        self.assertEqual(len(MBWay.objects.all()), 1)
        self.assertEqual(len(Payment.objects.all()), 2)
